use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use log::{info, warn};
use chrono::Utc;
use env_logger::Env;

#[derive(Deserialize, Debug)] // Ensure Debug is derived for easier logging
struct OrderRequest {
    item_id: String,
    quantity: u32,
}

#[derive(Serialize)]
struct OrderResponse {
    message: String,
    success: bool,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    // Set up the environment logger
    env_logger::Builder::from_env(Env::default().default_filter_or("info"))
        .init();

    lambda_runtime::run(handler_fn(process_order)).await?;
    Ok(())
}

async fn process_order(request: OrderRequest, _: Context) -> Result<OrderResponse, Error> {
    // Log statements now use Utc::now() directly within the log message
    info!("{} Received order request: {:?}", Utc::now().to_rfc3339(), request);

    if request.quantity == 0 {
        warn!("{} Invalid order quantity for item: {}", Utc::now().to_rfc3339(), request.item_id);
        return Ok(OrderResponse { message: "Invalid order quantity".to_string(), success: false });
    }

    // Continue with the function's logic
    info!("{} Processing payment for item: {}", Utc::now().to_rfc3339(), request.item_id);
    Ok(OrderResponse { message: "Order processed successfully".to_string(), success: true })
}
