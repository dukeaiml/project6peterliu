# Ecommerce Lambda with Logging and Tracing


## Table of Contents
1. [Deliverables](#deliverables)
2. [Description](#description)
3. [Installation and Usage](#Installation and Usage)
4. [Development Process](#development-process)

## Deliverables

Log Group Details (CloudWatch)

![Story Screenshot](/Screenshot_2024-03-08_at_6.33.37_PM.png)

X-ray Tracing
![GET Screenshot](/Screenshot_2024-03-08_at_6.34.46_PM.png)

Detailed CloudWatch Logs
![CloudWatch Logs](/Screenshot_2024-03-08_at_6.35.14_PM.png)

## Description

The rust_lambda_ecommerce AWS Lambda function is developed in Rust to manage e-commerce order processing. On receiving a request, it first validates the order details, such as item identifiers and quantities, to ensure they meet the required criteria. If the validation fails, it logs the error and terminates the operation, returning a failure response to the requester.

If the order details are valid, the function then simulates a payment process, a placeholder for real-world payment integrations, adjusting the simulation based on the order specifics. It logs each step of the process, providing visibility into the operation's flow and outcome.

The function uses AWS CloudWatch for logging, storing detailed information about each invocation, including execution time, input, output, and errors. This aids in debugging and monitoring the service's health and performance. AWS X-Ray integration provides insights into the function's behavior, showing how requests are processed and highlighting any performance bottlenecks.

## Installation and Usage

To run this project locally, ensure Rust, Cargo, and AWS CLI are installed on your system. Follow these installation steps:

1. Clone the repository:
git@gitlab.com:dukeaiml/project6peterliu.git

2. Set up AWS configs. 
Do 

cargo lambda build --release --target x86_64-unknown-linux-musl

Then do 

cargo lambda deploy

3. Then modify the cargo lambda permissions to include Xray, and modify the Logging to allow Xray and detailed CloudWatch logging.

4. Invoke the lambda function with

aws lambda invoke --function-name rust_lambda_ecommerce --payload file://input.json response.json

After invoking the function, you can view detailed logs in AWS CloudWatch and trace data in AWS X-Ray through the AWS Management Console


## Development Process

Development steps included:

1. Environment Setup: Establishing Rust environment and dependencies.
2. Implementation: Writing code to integrate with Rust Lambda and Logs for Appropriate Log Tracing.
3. Amazon Side Work: Connecting Lambda to Cloudwatch and Xray and Modifying IAM permissions.
3. Testing: Ensuring functionality and handling errors.
4. Documentation: Documenting code and writing this README.


